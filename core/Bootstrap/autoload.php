<?php

declare(strict_types=1);

use Phalcon\Loader;

$loader = new Loader();
$namespaces = [
    //    'Phalcon\Api'                 => \Core\appPath('/library'),
    //    'Phalcon\Api\Api\Controllers' => \Core\appPath('/api/controllers'),
    //    'Phalcon\Api\Cli\Tasks'       => \Core\appPath('/cli/tasks'),
    //    'Phalcon\Api\Tests'           => \Core\appPath('/tests'),

    'App\Controllers' => \Core\appPath('/Controllers'),

];

$loader->registerNamespaces($namespaces);
$loader->register();

// Load environment
//(new Dotenv(\Core\appPath()))->overload();
$dotenv = \Dotenv\Dotenv::createMutable(\Core\appPath());
$dotenv->load();
