<?php

namespace Core\Application;

use function Core\appPath;
use Core\Bootstrap\AbstractBootstrap;
use function Core\Config;
use Core\Exceptions\InvoException;
use Phalcon\Di\FactoryDefault;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;

class InvoApplication extends AbstractBootstrap
{
    public function run()
    {
        try {
            if (Config('app.app_debug')) {
                ErrorHandler::register();
                ExceptionHandler::register();
            }

            $this->application->useImplicitView(false);

            return $this->application->handle($_SERVER['REQUEST_URI']);
        } catch (\Exception $exception) {
            $response = $this->getResponse();

            // 如果没有找到对应的 router的处理
            if ($exception instanceof \Phalcon\Mvc\Dispatcher\Exception) {
                return $response->setJsonContent(
                    ['code' => 404, 'message' => $exception->getMessage(), 'data' => new \stdClass()]
                )->setStatusCode(404)->send();
            }

            if ($exception instanceof InvoException) {
                return $response->setJsonContent(
                    ['code' => $exception->getCode(), 'message' => $exception->getMessage(), 'data' => new \stdClass()]
                )->send();
            }

            \Core\Logger()->error($exception->getMessage());

            if (Config('app.app_debug')) {
                throw $exception;
            }

            return $response->setJsonContent(
                [
                    'code'      => 100001,
                    'message'   => 'sys.node.node_module',
                    'error_msg' => $exception->getMessage(),
                    'data'      => new \stdClass(), ]
            )->send();
        }
    }

    /**
     * @return mixed
     */
    public function setup()
    {
        $this->container = new FactoryDefault();

        $this->providers = require appPath('app/Config/providers.php');
        parent::setup();
    }
}
