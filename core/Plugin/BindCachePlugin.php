<?php

namespace Core\Plugin;

class BindCachePlugin
{
    public static $KEY = '';

    public static $TTL = 0;

    private static $_instance = null;

    /**
     * 静态工厂方法，返还此类的唯一实例.
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
}
