<?php

namespace Core\Middleware;

use function Core\getResponse;
use Phalcon\Di\Injectable;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;

class NotFoundMiddleware extends Injectable implements MiddlewareInterface
{
    /**
     * Checks if the resource was found.
     */
    public function beforeNotFound()
    {
        $this->halt(
            $this->application,
            404,
            'not found'
        );

        return false;
    }

    /**
     * Call me.
     *
     * @param Micro $api
     *
     * @return bool
     */
    public function call(Micro $api)
    {
        return true;
    }

    /**
     * Halt execution after setting the message in the response.
     *
     * @param Micro  $api
     * @param int    $status
     * @param string $message
     *
     * @return mixed
     */
    protected function halt(Micro $api, int $status, string $message)
    {
        /** @var Response $response */
        // $response = $api->getService('response');

        $api->stop();

        return;

        //var_dump($response);exit;
        getResponse()->setJsonContent([
            'code'    => $status,
            'message' => $message,
            'data'    => new \StdClass(),
        ])->send();

//        $response
//            ->setPayloadError($message)
//            ->setStatusCode($status)
//            ->send();

        $api->stop();
    }
}
