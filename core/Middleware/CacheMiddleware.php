<?php

namespace Core\Middleware;

use function Core\getCache;
use function Core\getResponse;
use Core\Plugin\BindCachePlugin;
use Phalcon\Di\Injectable;
use Phalcon\Mvc\Micro\MiddlewareInterface;

class CacheMiddleware extends Injectable implements MiddlewareInterface
{
    /**
     * Calls the middleware.
     *
     * @param \Phalcon\Mvc\Micro $application
     */
    public function call(\Phalcon\Mvc\Micro $application)
    {
        // TODO: Implement call() method.

        var_dump($application);
        exit;
    }

    public function beforeExecuteRoute()
    {
        $method_annotations = $this->annotations->getMethod(
            $this->dispatcher->getControllerClass(),
            $this->dispatcher->getActiveMethod()
        );

        // 检查是否方法中带有注释名称‘Cache’的注释单元
        $key = $this->request->getURI();

        // 如果有cache 的注释
        if ($method_annotations->has('Cache')) {
            $redis = getCache();

            if (!empty($data = $redis->get($key))) {
                getResponse()->setContent($data)->send();

                return false;
            }

            // 这个方法带有‘Cache’注释单元
            $annotation = $method_annotations->get('Cache');
            // 获取注释单元的‘lifetime’参数
            $lifetime = $annotation->getNamedParameter('ttl');

            if (empty($lifetime)) {
                $lifetime = 30;
            }

            $obj = BindCachePlugin::getInstance();
            $obj::$TTL = $lifetime;
            $obj::$KEY = $key;
        }
    }

    public function afterExecuteRoute()
    {
        $obj = BindCachePlugin::getInstance();

        // 如果需要设置cache
        if (!empty(trim($obj::$KEY))) {
            $statusCode = getResponse()->getStatusCode();

            if ($statusCode == 200) {
                $content = getResponse()->getContent();

                // if(isJson($content)) {

                $redis = getCache();

                $redis->set($obj::$KEY, $content, $obj::$TTL);
                // }
            }
        }
    }
}
