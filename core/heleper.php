<?php

declare(strict_types=1);

namespace Core;

use function function_exists;
use function getenv;
use Phalcon\Cache;
use Phalcon\Cache\Adapter\AdapterInterface as CacheAdapterInterface;
use Phalcon\Di\DiInterface;
use Phalcon\Http\Response;
use Phalcon\Logger;
use Phalcon\Mvc\Router;

if (true !== function_exists('Core\getResponse')) {
    function getResponse(): Response
    {
        return getDI()->getShared('response');
    }
}

if (true !== function_exists('Core\request')) {
    function getRequest()
    {
        return getDI()->get('request');
    }
}

if (true !== function_exists('Core\Logger')) {
    function Logger(): Logger
    {
        return getDI()->get('log');
    }
}

if (true !== function_exists('Core\SuccessResponse')) {
    function SuccessResponse($data)
    {
        return getResponse()->setJsonContent(
            [
                'code'    => 200,
                'message' => 'success',
                'data'    => $data,
            ]
        )->setStatusCode(200)->send();
    }
}

if (true !== function_exists('Core\Config')) {
    function Config()
    {
        $args = func_get_args();

        $config = getDI()->getShared('config');

        if (empty($args)) {
            return $config;
        }

        return call_user_func_array(
            [$config, 'path'],
            $args
        );
    }
}

if (true !== function_exists('Core\validation')) {
    function validationRun($rules, $data)
    {
        $validation = getDI()->get('validation');

        $validation->run($rules, $data);
    }
}

if (true !== function_exists('Core\getDI')) {
    /**
     * Get the application path.
     *
     * @param string $path
     *
     * @return string
     */
    function getDI(string $path = ''): DiInterface
    {
        return \Phalcon\Di::getDefault();
    }
}

if (true !== function_exists('Core\getCache')) {
    /**
     * Get the application path.
     *
     *
     * @return Cache
     */
    function getCache(): CacheAdapterInterface
    {
        return getDI()->getShared('cache');
    }
}

if (true !== function_exists('Core\getRouter')) {
    /**
     * Get the application path.
     *
     * @param string $path
     *
     * @return string
     */
    function getRouter(): Router
    {
        return getDI()->get('router');
    }
}
if (true !== function_exists('Core\appPath')) {
    /**
     * Get the application path.
     *
     * @param string $path
     *
     * @return string
     */
    function appPath(string $path = ''): string
    {
        return dirname(__DIR__).($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}
//if (true !== function_exists('Core\appPath')) {
//    /**
//     * Gets a variable from the environment, returns it properly formatted or the
//     * default if it does not exist
//     *
//     * @param string     $variable
//     * @param mixed|null $default
//     *
//     * @return mixed
//     */
//    function envValue(string $variable, $default = null)
//    {
//        $return = $default;
//        $value  = getenv($variable);
//        $values = [
//            'false' => false,
//            'true'  => true,
//            'null'  => null,
//        ];
//
//        if (false !== $value) {
//            $return = $values[$value] ?? $value;
//        }
//
//        return $return;
//    }
//}
//if (true !== function_exists('Core\envValue')) {
//    /**
//     * Gets a variable from the environment, returns it properly formatted or the
//     * default if it does not exist
//     *
//     * @param string     $variable
//     * @param mixed|null $default
//     *
//     * @return mixed
//     */
//    function envValue(string $variable, $default = null)
//    {
//        $return = $default;
//        $value  = getenv($variable);
//        $values = [
//            'false' => false,
//            'true'  => true,
//            'null'  => null,
//        ];
//
//        if (false !== $value) {
//            $return = $values[$value] ?? $value;
//        }
//
//        return $return;
//    }
//}

if (true !== function_exists('Core\appUrl')) {
    /**
     * Constructs a URL for links with resource and id.
     *
     * @param string $resource
     * @param int    $recordId
     *
     * @return array|false|mixed|string
     */
    function appUrl(string $resource, int $recordId)
    {
        return sprintf(
            '%s/%s/%s',
            envValue('APP_URL'),
            $resource,
            $recordId
        );
    }
}
