<?php
/**
 * Created by PhpStorm.
 * User: luoxun
 * Date: 20-1-14
 * Time: 下午4:25.
 */

namespace Core\Package;

use Core\Exceptions\ValidationExceptions;
use Phalcon\Validation\Validator;
use PhalconExt\Validation\Validation as BaseExtValidation;
use PhalconExt\Validators;

class Validation extends BaseExtValidation
{
    /** @var array The alias of available validators */
    protected $_validators = [
        'alnum'        => Validator\Alnum::class,
        'alpha'        => Validator\Alpha::class,
        'between'      => Validator\Between::class,
        'confirmation' => Validator\Confirmation::class,
        'creditcard'   => Validator\CreditCard::class,
        'date'         => Validator\Date::class,
        'digit'        => Validator\Digit::class,
        'email'        => Validator\Email::class,
        'not_in'       => Validator\ExclusionIn::class,
        'file'         => Validator\File::class,
        'identical'    => Validator\Identical::class,
        'in'           => Validator\InclusionIn::class,
        'numeric'      => Validator\Numericality::class,
        'required'     => Validator\PresenceOf::class,
        'regex'        => Validator\Regex::class,
        'length'       => Validator\StringLength::class,
        'unique'       => Validator\Uniqueness::class,
        'url'          => Validator\Url::class,
        'exist'        => Existence::class,
    ];

    /**
     * Add all the rules for given attribute to validators list.
     *
     * @param string $attribute
     * @param array  $rules
     *
     * @return void
     */
    protected function attributeRules(string $attribute, array $rules)
    {
        foreach ($rules as $rule => $options) {
//            var_dump($this->getValidators());
//            var_dump($this->_validators);
//            exit;
            if (!isset($this->_validators[$rule])) {
                throw new \InvalidArgumentException('Unknown validation rule: '.$rule);
            }

            $validator = $this->_validators[$rule];
            $options = (array) $options + [
                'callback'     => $this->callbacks[$rule] ?? null,
                'message'      => $this->_defaultMessages[$rule] ?? null,
                '__field'      => $attribute,
                'cancelOnFail' => true,
            ];

//            $validation->add(
//                'url',
//                new Validation\Validator\PresenceOf(
//
//                )
//            );
//            var_dump($options);
//            var_dump($validator);

            //exit;
//            $this->add($attribute,new Validator\PresenceOf(['message'=>'dddd']));

            $this->add($attribute, new $validator($options));
        }
    }

    public function register(string $ruleName, $handler, string $message = ''): self
    {
        if (isset($this->_validators[$ruleName])) {
            return $this;
        }

        if ($message) {
            $this->_defaultMessages += [$ruleName => $message];
        }

        $this->validators[$ruleName] = $this->getHandler($ruleName, $handler);

        return $this;
    }

    /** @var array The alias of available validators */

    /**
     * self validation with given ruleSet against given arbitrary dataSet.
     *
     * @param array        $ruleSet
     * @param array|object $dataSet
     *
     * @return self
     */
    public function run(array $ruleSet, $dataSet): self
    {

//        $dd =  $this->validate($dataSet);
//
//        var_dump($this);
//        exit;
//        $this->messages = $this->validators = [];

        // See if it is arrayable!
        if (\is_object($dataSet)) {
            $dataSet = $this->prepareDate($dataSet);
        }

        // OK, must be entity!
        if (\is_object($dataSet)) {
            //$this->_entity = $dataSet;
            $this->setEntity($dataSet);
        } else {
            $this->data = $dataSet;
        }

        $messages = $this->addRules($ruleSet)->validate();

        if (count($messages) > 0) {
            throw  new  ValidationExceptions($messages[0]);
        }

        return $this;
    }
}
