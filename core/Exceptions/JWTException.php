<?php
/**
 * Created by PhpStorm.
 * User: luoxun
 * Date: 20-1-15
 * Time: 下午12:30.
 */

namespace Core\Exceptions;

class JWTException extends InvoException
{
    protected $code = '200202';
}
