<?php
/**
 * Created by PhpStorm.
 * User: luoxun
 * Date: 20-1-14
 * Time: 下午10:12.
 */

namespace Core\Exceptions;

class ValidationExceptions extends InvoException
{
    // 验证默认错误
    protected $code = '200201';
}
