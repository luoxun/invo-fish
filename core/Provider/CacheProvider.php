<?php
/**
 * Created by PhpStorm.
 * User: luoxun
 * Date: 20-5-20
 * Time: 上午1:34.
 */

namespace Core\Provider;

use function Core\Config;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Storage\SerializerFactory;

class CacheProvider implements ServiceProviderInterface
{
    /**
     * Registers a service provider.
     *
     * @param DiInterface $di
     *
     * @return void
     */
    public function register(DiInterface $di): void
    {
        // TODO: Implement register() method.

        $di->setShared('cache', function () {
            $serializer = new SerializerFactory();

            // $redis = new \Phalcon\Cache\Adapter\Redis(
            $redis = new \Phalcon\Cache\Adapter\Memory(
                $serializer,
                [

                    'defaultSerializer' => 'msgpack',
                    'lifetime'          => 60,
                    'host'              => Config('redis.host'),
                    'port'              => Config('redis.port'),
                    'index'             => Config('redis.index'),

                ]
            );

            return $redis;
        });
    }
}
