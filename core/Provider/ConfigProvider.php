<?php

namespace Core\Provider;

use Phalcon\Config;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class ConfigProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'config',
            function () {
                $handler = opendir(APP_PATH.'app/Config');

                $config_files = [];

                while (($filename = readdir($handler)) !== false) {
                    if ($filename != '.' && $filename != '..') {
                        $filename_no_suffix = str_replace(strrchr($filename, '.'), '', $filename);

                        $config_files[] = [
                            'adapter'  => 'php',
                            'filePath' => APP_PATH.'app/Config/'.$filename,
                            //'config' => $filename_no_suffix
                        ];
                    }
                }

                closedir($handler);

//                var_dump($config_files);

                //$dd = new Config\Adapter\Grouped($config_files);

                return new Config\Adapter\Grouped($config_files);
            }
        );
    }
}
