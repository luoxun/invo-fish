<?php

namespace Core\Provider;

use Core\Http\Response;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class ResponseProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register($container): void
    {
        $container->setShared(
            'response',
            function () {
                $response = new Response();
                //$response = new Response();
                //$response = new Response();

                /**
                 * Assume success. We will work with the edge cases in the code.
                 */
                $response
                    ->setStatusCode(200)
                    ->setContentType('application/vnd.api+json', 'UTF-8');

                return $response;
            }
        );
    }
}
