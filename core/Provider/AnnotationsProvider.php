<?php

namespace Core\Provider;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class AnnotationsProvider implements ServiceProviderInterface
{
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'annotations',
            function () {
                return new \Phalcon\Annotations\Adapter\Apcu();
            }
        );
    }
}
