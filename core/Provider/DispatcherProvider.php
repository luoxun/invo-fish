<?php

namespace Core\Provider;

use Core\Middleware\CacheMiddleware;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Events\Manager;
use Phalcon\Mvc\Dispatcher;

class DispatcherProvider implements ServiceProviderInterface
{
    /**
     * Registers a service provider.
     *
     * @param DiInterface $di
     *
     * @return void
     */
    public function register(DiInterface $di): void
    {
        // TODO: Implement register() method.

        $di->setShared(
            'dispatcher',
            function () {
                $eventsManager = new Manager();

                $eventsManager->attach('dispatch:beforeExecuteRoute', new CacheMiddleware());

                $eventsManager->attach('dispatch:afterExecuteRoute', new CacheMiddleware());

                $dispatcher = new Dispatcher();

                $dispatcher->setEventsManager($eventsManager);

                return $dispatcher;
            }
        );
    }
}
