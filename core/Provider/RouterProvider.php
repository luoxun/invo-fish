<?php

declare(strict_types=1);

namespace Core\Provider;

use function Core\appPath;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Events\Manager;
use Phalcon\Mvc\Application;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Router;

class RouterProvider implements ServiceProviderInterface
{
    /**
     * Registers a service provider.
     *
     * @param DiInterface $container
     *
     * @return void
     */
    public function register(DiInterface $container): void
    {
        /** @var Micro $application */
        $application = $container->getShared('application');
        /** @var Manager $eventsManager */
        $eventsManager = $container->getShared('eventsManager');

        $router = $this->attachRoutes($application);

        $container->setShared(
            'router',
            function () use ($router) {
                return $router;
            }
        );
    }

    /**
     * Attaches the middleware to the application.
     *
     * @param Micro   $application
     * @param Manager $eventsManager
     */
    private function attachMiddleware(Application $application, Manager $eventsManager)
    {
//        $middleware = $this->getMiddleware();

        /**
         * Get the events manager and attach the middleware to it.
         */
//        foreach ($middleware as $class => $function) {
//
//                    $eventsManager->attach('micro', new $class());
//
//
//
//                    $application->{$function}(new $class());
//
//
//                }
    }

    /**
     * Attaches the routes to the application; lazy loaded.
     *
     * @param Application $application
     *
     * @return Router\Annotations;
     */
    private function attachRoutes(Application $application): Router\Annotations
    {

        //$router = new Router\Annotations(true);

        $router = include appPath('/app/Router/router.php');

        return $router;
//
       //// Define a route
        //        $router->add(
        //            '/demo',
        //            [
        //               // 'namespace' => '',
        //                'controller' => 'App\Controllers\Index',
        //                'action'     => 'Index',
        //            ]
        //        );

//        $router->handle('/demo');
        //
        //
        //
        //        // Check if some route was matched
        //        if ($router->wasMatched()) {
        //            echo 'Controllers: ', $router->getControllerName(), '<br>';
        //            echo 'Action: ', $router->getActionName(), '<br>';
        //        } else {
        //            echo "The route wasn't matched by any route<br>";
        //        }
        //
        //
        //        var_dump($router);exit;

//        var_dump( $router->getMatchedRoute());
        //        return ;
        // $routes = $this->getRoutes();

//        var_dump($application->getRouter());exit;
        //
        //        foreach ($routes as $route) {
        //            $collection = new Collection();
        //            $collection
        //                ->setHandler($route[0], true)
        //                ->setPrefix($route[1])
        //                ->{$route[2]}($route[3], 'callAction');
        //
        //            $application->mount($collection);
        //        }
    }
}
