<?php
/**
 * Created by PhpStorm.
 * User: luoxun
 * Date: 20-5-19
 * Time: 上午1:43.
 */

namespace Core\Provider;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\Stream;

class LogProvider implements ServiceProviderInterface
{
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'log',
            function () {
                $dt = new \DateTime();

                $adapter = new Stream(
                    APP_PATH.'storage/logs/'.$dt->format('Y-m-d').'.log',
                    [
                        'mode' => 'a',
                    ]
                );

                return new Logger(
                    'messages',
                    [
                        'main' => $adapter,
                    ]
                );
            }
        );
    }
}
