<?php
/**
 * Created by PhpStorm.
 * User: luoxun
 * Date: 20-1-13
 * Time: 下午8:52.
 */

namespace Core\Provider;

use Core\Package\Validation;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class ValidationProvider implements ServiceProviderInterface
{
    /**
     * Registers a service provider.
     *
     * @param DiInterface $container
     *
     * @return void
     */
    public function register(DiInterface $container): void
    {
//       $container->setShared('validation', Validation::class);
        $container->setShared(
            'validation',
            function () {

                // exit;
                $validation = new Validation();

//                $validation->setValidators(
                //                    [
                //                        'required'     => Validator\PresenceOf::class,
                //                        'alnum'        => Validator\Alnum::class,
                //                        'alpha'        => Validator\Alpha::class,
                //                        'between'      => Validator\Between::class,
                //                        'confirmation' => Validator\Confirmation::class,
                //                        'creditcard'   => Validator\CreditCard::class,
                //                        'date'         => Validator\Date::class,
                //                        'digit'        => Validator\Digit::class,
                //                        'email'        => Validator\Email::class,
                //                        'not_in'       => Validator\ExclusionIn::class,
                //                        'file'         => Validator\File::class,
                //                        'identical'    => Validator\Identical::class,
                //                        'in'           => Validator\InclusionIn::class,
                //                        'numeric'      => Validator\Numericality::class,
                //                        'required'     => PresenceOf::class,
                //                        'regex'        => Validator\Regex::class,
                //                        'length'       => Validator\StringLength::class,
                //                    ]
                //                );

                return $validation;
            }
        );
    }
}
