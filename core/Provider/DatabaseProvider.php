<?php

namespace Core\Provider;

use Illuminate\Database\Capsule\Manager as Capsule;
use Phalcon\Di\DiInterface;
//use function Phalcon\Api\Core\envValue;

use Phalcon\Di\ServiceProviderInterface;

class DatabaseProvider implements ServiceProviderInterface
{
    /**
     * @param DiInterface $container
     */
    public function register(DiInterface $container): void
    {
        $container->setShared(
            'db',
            function () {
                        $capsule = new Capsule();

                        $db_config = \Core\Config('db');

                        foreach ($db_config as $value) {
                            $capsule->addConnection($value->toArray());
                        }

                        $capsule->setAsGlobal();

                        $capsule->bootEloquent();

                        return $capsule->getDatabaseManager();
                    }
        );
    }
}
