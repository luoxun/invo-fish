<?php

namespace  Core\Models;

use function Core\getDI;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BaseModel extends Model
{
    // 使用软删
    use SoftDeletes;
    // 使用时间戳
    public $timestamps = true;

    /**
     * Get the current connection name for the model.
     *
     * @return string|null
     */
    public function getConnection()
    {
        $db = getDI()->get('db');

        return $db;
    }
}
