<?php

ini_set('date.timezone', 'Asia/Shanghai');

require __DIR__.'/../vendor/autoload.php';

error_reporting(E_ALL);

ini_set('display_errors', 1);

define('APP_PATH', realpath('..').'/');

require_once __DIR__.'/../core/Bootstrap/autoload.php';

$app = new \Core\Application\InvoApplication();
$app->setup();
$app->run();
