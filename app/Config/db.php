<?php

return [

    'db'=> [

        'default' => [
            'driver' => 'mysql',

            'host' => getenv('DB_HOST'),

            'username' => getenv('DB_USER'),

            'password' => getenv('DB_PWD'),

            'database' => getenv('DB_NAME'),

            'charset' => 'utf8mb4',

            'collation' => 'utf8mb4_unicode_ci',

            'port' => 3306,
        ],
    ],
];
