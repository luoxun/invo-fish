<?php
/**
 * Created by PhpStorm.
 * User: luoxun
 * Date: 20-5-20
 * Time: 上午1:30.
 */

return [
    'redis' => [
        'host'  => getenv('REDIS_HOST', '127.0.0.1'),
        'port'  => getenv('REIS_PORT', 6379),
        'index' => 0,
    ],
];
