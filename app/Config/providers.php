<?php

/**
 * Enabled providers. Order does matter.
 */
return [

    //    \Core\Provider\AnnotationsProvider::class,

    \Core\Provider\ConfigProvider::class,

    \Core\Provider\DatabaseProvider::class,

    //\Core\Provider\ResponseProvider::class,

    \Core\Provider\DispatcherProvider::class,

    \Core\Provider\RouterProvider::class,
    \Core\Provider\ValidationProvider::class,
    \Core\Provider\LogProvider::class,
    \Core\Provider\CacheProvider::class,

];
