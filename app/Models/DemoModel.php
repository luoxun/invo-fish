<?php

namespace App\Models;

use Core\Models\BaseModel;
use Phalcon\Mvc\Model;

/**
 * Class DemoModel.
 *
 * @method BaseModel first
 * @method string test
 */
class DemoModel extends BaseModel
{
    protected $table = 'original';

//    public function initialize()
//    {
//        $this->setSource('demo');
//
//        $this->addBehavior(
//            new Model\Behavior\SoftDelete(
//                [
//                    'field' => 'inv_deleted_flag',
//                    'value' => DemoModel::INACTIVE,
//                ]
//            )
//        );
//    }
}
