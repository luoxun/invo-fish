<?php

//namespace App\Router;

$router = new \Phalcon\Mvc\Router\Annotations(false);
$router->setDefaultNamespace('App\Controllers');

$router->notFound([
    'controller' => 'Index',
    'action'     => 'notFound',
]);

// 路由
$router->addResource("App\Controllers\Example\Demo");

//$router->addResource("App\Controllers\Token\Token", '/token');

return $router;
