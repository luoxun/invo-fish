<?php

namespace App;

class Kernel
{
    public $beforeExecuteRoute = [

    ];

    public $beforeHandleRoute = [

    ];

    public $afterExecuteRoute = [

    ];

    public $afterHandleRoute = [

    ];
}
