<?php
/**
 * Created by PhpStorm.
 * User: luoxun
 * Date: 20-1-13
 * Time: 下午11:44.
 */

namespace App\Controllers;

use Carbon\Carbon;
use function Core\getRequest;
use function Core\getResponse;
use Core\Http\Controller\BaseController;
use function Core\SuccessResponse;

class IndexController extends BaseController
{
    public function indexAction()
    {
        $ip = getRequest()->getClientAddress();

        // $ip_info = file_get_contents("http://freeapi.ipip.net/".$ip);

        //exit;

        return SuccessResponse(['version' => '0.0.1', 'srv_time' => Carbon::now(), 'ip' => $ip]);
        // return SuccessResponse(['version'=>'0.0.1','srv_time'=>Carbon::now(),"ip"=> json_decode($ip_info,true)]);
    }

    public function notFoundAction()
    {
        $ip = getRequest()->getClientAddress();

        return SuccessResponse(['version' => '0.0.1', 'srv_time' => Carbon::now(), 'ip' => $ip]);

        return getResponse()->setStatusCode(404)->setJsonContent([
            'code'   => 404,
            'message'=> '404',
        ])->send();

        return SuccessResponse(['version' => '0.0.1', 'srv_time' => Carbon::now(), 'ip' => $ip]);
        // return SuccessResponse(['version'=>'0.0.1','srv_time'=>Carbon::now(),"ip"=> json_decode($ip_info,true)]);
    }
}
