<?php

declare(strict_types=1);

namespace App\Controllers\Example;

use App\Models\DemoModel;
use Core\Http\Controller\BaseController;

/**
 * Class DemoController.
 *
 *
 * @RoutePrefix("/example")
 */
class DemoController extends BaseController
{
    /**
     * @Get(
     *     "/getone"
     * )
     *
     * @Cache( ttl=300 )
     *
     * @throws \Phalcon\Cache\Exception\InvalidArgumentException
     *
     * @return mixed
     */
    public function OneAction()
    {
        return \Core\SuccessResponse(DemoModel::first());
    }

    /**
     * @Get(
     *     "/getone2"
     * )
     *
     * @return mixed
     */
    public function One2Action()
    {
        return \Core\SuccessResponse(['google'=>'google']);
    }
}
